const express = require('express');
const app = express();

app.get('/healthcheck', (req, res, next) => {
    res.json({
        success: true,
        message: 'APIs are up'
    })
})

app.get('/helloworld', (req, res, next) => {
    res.json({
        success: true,
        message: 'Hello World'
    })
})

app.listen(4000, () => {
    console.log('Server is running');
})

module.exports = app;