var request = require('supertest');
var app = require('../server');

describe('GET /', function() {   
    it('healthcheck', function(done) {
             // The line below is the core test of our app.     
             request(app).get('/healthcheck').expect({
                success: true,
                message: 'APIs are up'
            }, done);   
    }); 
});

describe('GET /', function() {   
    it('Hello world', function(done) {
             // The line below is the core test of our app.     
             request(app).get('/helloworld').expect({
                success: true,
                message: 'Hello World'
            }, done);   
    }); 
});